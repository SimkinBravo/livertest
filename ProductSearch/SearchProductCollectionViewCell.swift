//
//  SearchProductCollectionViewCell.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

import UIKit
import Cartography

class SearchProductCollectionViewCell: UICollectionViewCell {

    static public let reuseId = "search-product-cvc"

    let titleLabel = UILabel()
    let priceLabel = UILabel()
    let locationLabel = UILabel()
    let previewImageView = UIImageView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        initElements()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Interface Builder is not supported!")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        fatalError("Interface Builder is not supported!")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        titleLabel.text = ""
        priceLabel.text = ""
        locationLabel.text = ""
        previewImageView.image = nil
    }

    private func initElements() {

        previewImageView.backgroundColor = .green

        contentView.addSubview(titleLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(locationLabel)
        contentView.addSubview(previewImageView)

        let margin: CGFloat = 10

        constrain(titleLabel, priceLabel, locationLabel, previewImageView) { title, price, location, preview in

            let container = preview.superview!

            preview.left == container.left + margin
            preview.width == preview.height
            preview.top == container.top + margin
            preview.bottom == container.bottom - margin

            title.left == preview.right + margin
            title.right == container.right - margin
            title.top == preview.top

            price.left == title.left
            price.right == title.right
            price.top == title.bottom

            location.left == price.left
            location.right == price.right
            location.top == price.bottom
        }
    }
}
