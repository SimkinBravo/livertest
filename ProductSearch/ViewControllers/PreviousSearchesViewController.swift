//
//  PreviousSearchesViewController.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

import UIKit
import Cartography

class PreviousSearchesViewController: UIViewController {

    var didSelectSearch: ((String) -> Void)?

    let dataSource = PreviousSearchesDataSource()

    lazy var collectionView: UICollectionView = {

        return fetchCollectionView()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        title = "Últimas búsquedas"
        configureCollectionView()
        loadData()
    }

    private func loadData() {

        let searches = UserDataManager.getPreviousSearches()
        dataSource.set(previousSearches: searches)
    }
    

    private func configureCollectionView() {

        collectionView.dataSource = dataSource
        collectionView.delegate = self
        dataSource.registerView(for: collectionView)

        dataSource.collectionUpdate = { [weak self] in

            self?.collectionView.reloadData()
        }
    }

    internal func fetchCollectionView() -> UICollectionView {

        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        view.addSubview(collectionView)

        constrain(collectionView) { collection in

            collection.edges == collection.superview!.edges
        }
        return collectionView
    }
}

extension PreviousSearchesViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: UIScreen.main.bounds.width, height: 40)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("PreviousSearchesViewController", indexPath)

        guard let keyword = dataSource.previousSearches?.searches[indexPath.row] else { return }
        
        print("PreviousSearchesViewController", indexPath)
        didSelectSearch?(keyword)
    }
}
