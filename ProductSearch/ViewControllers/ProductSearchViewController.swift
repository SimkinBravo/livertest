//
//  ProductSearchViewController.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

import UIKit
import Cartography

class ProductSearchViewController: UIViewController {

    var presentPreviousSearchesView: (() -> Void)?

    let dataSource = SearchProductDataSource()
    var lastKeyword = ""

    lazy var collectionView: UICollectionView = {

        return fetchCollectionView()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Búsqueda de producto"
        view.backgroundColor = .white
        configureCollectionView()
    }

    func searchProduct(keyword: String, page: Int) {

        lastKeyword = keyword
        dataSource.currentPage = page
        RestApi(loader: AlamofireLoader()).searchProduct(keyword: keyword, page: page, items: dataSource.numberOfItemsPerPage) { [weak self] response in

            if page == 1 {
                UserDataManager.save(keyword: keyword)
            }
            switch response {
            case .success(let products):
                if page == 1 {
                    self?.dataSource.set(products: products)
                } else {
                    self?.dataSource.add(products: products)
                }

            case .failure(let error):
                self?.displayError(error: error.localizedDescription)
            }
            
        }
    }

    private func displayError(error: String) {
        print("Hubo un error ", error)
    }

    private func configureCollectionView() {

        collectionView.dataSource = dataSource
        collectionView.delegate = self
        dataSource.registerView(for: collectionView)

        dataSource.searchDataDidChange = { [weak self] keyword in

            self?.searchProduct(keyword: keyword, page: 1)
        }

        dataSource.previousSearchesViewDidRequest = { [weak self] in

            self?.presentPreviousSearchesView?()
        }

        dataSource.collectionUpdate = { [weak self] in

            self?.collectionView.reloadData()
        }

        dataSource.loadNextPage = { [weak self] page in

            guard let keyword = self?.lastKeyword else { return }
            self?.searchProduct(keyword: keyword, page: page)
        }
    }

    internal func fetchCollectionView() -> UICollectionView {

        let layout = UICollectionViewFlowLayout()
        layout.sectionHeadersPinToVisibleBounds = true
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        view.addSubview(collectionView)

        constrain(collectionView) { collection in

            collection.edges == collection.superview!.edges
        }
        return collectionView
    }
}

extension ProductSearchViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: UIScreen.main.bounds.width, height: 100)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 150)
    }
}
