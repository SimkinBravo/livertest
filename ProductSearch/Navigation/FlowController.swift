//
//  FlowController.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

import UIKit

class FlowController {

    let navigationController: UINavigationController?
    let searchController = ProductSearchViewController()

    init(navigationController: UINavigationController) {

        self.navigationController = navigationController
    }

    func presentProductSearchView() {
        print("presentUserProfile", navigationController)

        navigationController?.setViewControllers([searchController], animated: true)
        searchController.presentPreviousSearchesView = {
            print("prodcutSearchView.presentPreviousSearchesView")
            self.presentPreviousSearchesView()
        }
    }

    func presentPreviousSearchesView() {

        guard let navigationController = navigationController else { return }
        let previousSearchesController = PreviousSearchesViewController()
        navigationController.pushViewController(previousSearchesController, animated: true)

        previousSearchesController.didSelectSearch = { [weak self] keyword in

            print("controller.didSelectSearch", navigationController)
            navigationController.popViewController(animated: true)
            self?.searchController.searchProduct(keyword: keyword, page: 1)
            self?.dismissView()
        }

    }

    func dismissView() {
        print("dismissView", navigationController)
//        navigationController.popViewController(animated: true)
    }
}
