//
//  UrlRouter.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

import UIKit

enum UrlRouter {

    enum ApiRequestRouter {

        case searchProduct(keyword: String, page: Int = 1, items: Int = 10)
    }

    case api(_ request: ApiRequestRouter)

    var url: URL? {

        switch self {
        case .api(let request):

            switch request {
            case .searchProduct(let keyword, let page, let numberOfItems):

                var urlComponents = getUrlComponents()
                urlComponents.queryItems = [
                   URLQueryItem(name: "force-plp", value: "true"),
                   URLQueryItem(name: "search-string", value: keyword),
                   URLQueryItem(name: "page-number", value: String(page)),
                   URLQueryItem(name: "number-of-items-per-page", value: String(numberOfItems))
                ]
                print(urlComponents.url?.absoluteString)
                return urlComponents.url
            }
        }
    }

    private func getUrlComponents() -> URLComponents {
        let scheme = "https"
        let host = "shoppapp.liverpool.com.mx"
        let path = "/appclienteservices/services/v3/plp"

        var urlComponents = URLComponents()
        urlComponents.scheme = scheme
        urlComponents.host = host
        urlComponents.path = path
        return urlComponents
    }

}
