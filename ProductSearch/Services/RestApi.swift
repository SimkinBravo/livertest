//
//  RestApi.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

import UIKit

protocol RequestLoader {

    func loadData<T>(_ type: T.Type, url: URL, then handler: @escaping (Result<T, Error>) -> Void) where T : Decodable
}

class RestApi {

    let loader: RequestLoader

    init(loader: RequestLoader) {
        self.loader = loader
    }

    func searchProduct(keyword: String, page: Int, items: Int, then handler: @escaping (Result<[SearchProduct], Error>) -> Void) {

        guard let url = UrlRouter.api(.searchProduct(keyword: keyword, page: page, items: items)).url else { return }

        loader.loadData(SearchProductResponse.self, url: url) { response in
            switch(response) {
            case .success(let data):
                handler(.success(data.results.records))
            case .failure(let error):
                handler(.failure(error))
            }
        }
    }
}
