//
//  AlamofireLoader.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

import UIKit
import Alamofire

struct GenericError: Error {

    let message: String
}

class AlamofireLoader: RequestLoader {

    func loadData<T>(_ type: T.Type, url: URL, then handler: @escaping (Result<T, Error>) -> Void) where T : Decodable {
        let request = URLRequest(url: url)

        AF.request(request).response { response in
            print(response)
            guard let data = response.data else {
                
                handler(.failure(GenericError(message: "No data. Response:")))
                return
            }

            do {
                let responseJSON = try JSONDecoder().decode(T.self, from: data)
                handler(.success(responseJSON))
            } catch(let error) {
                handler(.failure(error))
            }
        }
    }
}
