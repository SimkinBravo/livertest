//
//  SaveDiskManager.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

import UIKit

class UserDataManager {

    static func save(keyword: String, using fileManager: FileManager = .default) {

        var previousSearches = UserDataManager.getPreviousSearches()
        let folderURLs = fileManager.urls(for: .cachesDirectory, in: .userDomainMask)
        let fileURL = folderURLs[0].appendingPathComponent("user.cache")

        previousSearches.searches.append(keyword)
        print(previousSearches)
        do {
            let data = try JSONEncoder().encode(previousSearches)
            try data.write(to: fileURL)
        } catch(let error) {
            print(error)
        }
        print("saveToDisk")
    }

    static func getPreviousSearches(using fileManager: FileManager = .default) -> PreviousSearches {

        let folderURLs = fileManager.urls(for: .cachesDirectory, in: .userDomainMask)
        let fileURL = folderURLs[0].appendingPathComponent("user.cache")

        do {
            let data = try Data(contentsOf: fileURL)
            return try JSONDecoder().decode(PreviousSearches.self, from: data)
        } catch(_) {
            return PreviousSearches(searches: [])
        }
    }

    static func removeFromDisk(withName name: String, using fileManager: FileManager = .default) throws {

        let folderURLs = fileManager.urls(for: .cachesDirectory, in: .userDomainMask)

        let fileURL = folderURLs[0].appendingPathComponent("\(name).cache")
        try fileManager.removeItem(at: fileURL)
    }
}

struct PreviousSearches: Codable {

    var searches: [String]
}
