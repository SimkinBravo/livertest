//
//  ConfigureSearchProductViewCell.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

struct ConfigureProductCellView {

    func configure(view: SearchProductCollectionViewCell, with data: SearchProduct) {

        view.titleLabel.text = data.productDisplayName
        view.priceLabel.text = "$\(data.listPrice)"

        AF.request(data.smImage).responseImage { response in

            switch response.result {
            case .success(let image):
                view.previewImageView.image = image
            case .failure(_):
                print("failure")
            }
        }
    }
}
