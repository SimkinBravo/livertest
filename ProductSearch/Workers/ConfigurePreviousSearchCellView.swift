//
//  ConfigurePreviousSearchCellView.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

import UIKit

struct ConfigurePreviousSearchCellView {

    func configure(view: PreviousSearchCollectionViewCell, with str: String) {

        view.titleLabel.text = str
    }
}
