//
//  PreviousSearchCollectionViewCell.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

import UIKit
import Cartography

class PreviousSearchCollectionViewCell: UICollectionViewCell {

    static public let reuseId = "previous-search-cvc"

    let titleLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)

        initElements()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Interface Builder is not supported!")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        fatalError("Interface Builder is not supported!")
    }

    private func initElements() {

        let backgroundView = UIView()
        backgroundView.backgroundColor = .lightGray
        contentView.addSubview(backgroundView)
        contentView.addSubview(titleLabel)

        let margin: CGFloat = 10

        constrain(backgroundView, titleLabel) { back, title in

            let container = title.superview!

            title.left == container.left + margin
            title.right == container.right - margin
            title.top == container.top + margin
            title.bottom == container.bottom - margin

            back.edges == container.edges
        }
    }
}
