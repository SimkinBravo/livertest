//
//  ViewController.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

struct SearchProductResponse: Codable {

    enum CodingKeys: String, CodingKey {

        case results = "plpResults"
    }

    var results: SearchProductResult
}

struct SearchProductResult: Codable {

    var records: [SearchProduct]
}

struct SearchProduct: Codable {

    var productId: String
    var listPrice: Float
    var productDisplayName: String
    var smImage: String
}

/*
 titulo, precio, ubicación y Ia imagen (thumbnail).
{
   "status":{
      "status":"OK",
      "statusCode":0
   },
   "pageType":"PLP",
   "plpResults":{
      "label":"",
      "refinementGroups":[

      ],
      "records":[
         {
            "productId":"1084638958",
            "skuRepositoryId":"1084638958",
            "productDisplayName":"Pantalla LG HD de 32 Pulgadas, Modelo 32LM630BPUB",
            "productType":"Soft Line",
            "productRatingCount":343,
            "productAvgRating":4.8,
            "listPrice":7699.0,
            "minimumListPrice":7699.0,
            "maximumListPrice":7699.0,
            "promoPrice":4820.25,
            "minimumPromoPrice":4820.25,
            "maximumPromoPrice":4820.25,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"LG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1084638958.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1084638958.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1084638958.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1094820754",
            "skuRepositoryId":"1094820754",
            "productDisplayName":"Pantalla Samsung 4K UHD de 50 pulgadas, Modelo UN50TU8000FXZX",
            "productType":"Big Ticket",
            "productRatingCount":409,
            "productAvgRating":4.9,
            "listPrice":18899.0,
            "minimumListPrice":18899.0,
            "maximumListPrice":18899.0,
            "promoPrice":10998.75,
            "minimumPromoPrice":10998.75,
            "maximumPromoPrice":10998.75,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"SAMSUNG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1094820754_0p.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1094820754_0p.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1094820754_0p.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1096977979",
            "skuRepositoryId":"1096977979",
            "productDisplayName":"Pantalla Samsung 4K UHD de 65 pulgadas, Modelo UN65TU7000FXZX",
            "productType":"Big Ticket",
            "productRatingCount":102,
            "productAvgRating":4.7,
            "listPrice":21427.0,
            "minimumListPrice":21427.0,
            "maximumListPrice":21427.0,
            "promoPrice":16070.25,
            "minimumPromoPrice":16070.25,
            "maximumPromoPrice":16070.25,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"SAMSUNG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1096977979.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1096977979.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1096977979.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1082823260",
            "skuRepositoryId":"1082823260",
            "productDisplayName":"Pantalla TCL Full HD de 40 Pulgadas, Modelo 40A325",
            "productType":"Soft Line",
            "productRatingCount":140,
            "productAvgRating":4.6,
            "listPrice":9499.0,
            "minimumListPrice":9499.0,
            "maximumListPrice":9499.0,
            "promoPrice":5892.0,
            "minimumPromoPrice":5892.0,
            "maximumPromoPrice":5892.0,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"TCL",
            "seller":"Liverpool",
            "category":"Aparatos Electrónicos",
            "smImage":"https://ss629.liverpool.com.mx/sm/1082823260.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1082823260.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1082823260.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1096946542",
            "skuRepositoryId":"1096946542",
            "productDisplayName":"Pantalla Hisense 4K UHD de 50 pulgadas, Modelo 50H6500G",
            "productType":"Big Ticket",
            "productRatingCount":39,
            "productAvgRating":4.8,
            "listPrice":19799.0,
            "minimumListPrice":19799.0,
            "maximumListPrice":19799.0,
            "promoPrice":8799.0,
            "minimumPromoPrice":8799.0,
            "maximumPromoPrice":8799.0,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"HISENSE",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1096946542.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1096946542.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1096946542.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1069454077",
            "skuRepositoryId":"1069454077",
            "productDisplayName":"Pantalla Samsung HD de 24 Pulgadas, Modelo LT24D310NQ/ZX",
            "productType":"Soft Line",
            "productRatingCount":81,
            "productAvgRating":4.8,
            "listPrice":3499.0,
            "minimumListPrice":3499.0,
            "maximumListPrice":3499.0,
            "promoPrice":2624.25,
            "minimumPromoPrice":2624.25,
            "maximumPromoPrice":2624.25,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"SAMSUNG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1069454077.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1069454077.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1069454077.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1095375363",
            "skuRepositoryId":"1095375363",
            "productDisplayName":"Pantalla Samsung HD de 32 pulgadas, Modelo UN32T4300AFXZX",
            "productType":"Soft Line",
            "productRatingCount":90,
            "productAvgRating":4.6,
            "listPrice":7999.0,
            "minimumListPrice":7999.0,
            "maximumListPrice":7999.0,
            "promoPrice":5355.75,
            "minimumPromoPrice":5355.75,
            "maximumPromoPrice":5355.75,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"SAMSUNG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1095375363_0p.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1095375363_0p.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1095375363_0p.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1084001631",
            "skuRepositoryId":"1084001631",
            "productDisplayName":"Pantalla LG SMART TV AI ThinQ FHD 43 Pulgadas 43LM6300PUB",
            "productType":"Soft Line",
            "productRatingCount":40,
            "productAvgRating":4.8,
            "listPrice":11570.0,
            "minimumListPrice":11570.0,
            "maximumListPrice":11570.0,
            "promoPrice":7499.25,
            "minimumPromoPrice":7499.25,
            "maximumPromoPrice":7499.25,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"LG",
            "seller":"Liverpool",
            "category":"Aparatos Electrónicos",
            "smImage":"https://ss629.liverpool.com.mx/sm/1084001631.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1084001631.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1084001631.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1097312024",
            "skuRepositoryId":"1097312024",
            "productDisplayName":"Pantalla LG UHD TV AI ThinQ 4K de 50 pulgadas, Modelo 50UN7300PUC",
            "productType":"Big Ticket",
            "productRatingCount":28,
            "productAvgRating":4.8,
            "listPrice":18999.0,
            "minimumListPrice":18999.0,
            "maximumListPrice":18999.0,
            "promoPrice":10177.5,
            "minimumPromoPrice":10177.5,
            "maximumPromoPrice":10177.5,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"LG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1097312024.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1097312024.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1097312024.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1086877194",
            "skuRepositoryId":"1086877194",
            "productDisplayName":"Pantalla Hisense HD de 32 Pulgadas, Modelo 32H5500F",
            "productType":"Soft Line",
            "productRatingCount":131,
            "productAvgRating":4.8,
            "listPrice":7399.0,
            "minimumListPrice":7399.0,
            "maximumListPrice":7399.0,
            "promoPrice":4799.0025,
            "minimumPromoPrice":4799.0025,
            "maximumPromoPrice":4799.0025,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"HISENSE",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1086877194.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1086877194.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1086877194.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1097746024",
            "skuRepositoryId":"1097746024",
            "productDisplayName":"Pantalla TCL 4K UHD de 50 pulgadas, Modelo 50A435",
            "productType":"Big Ticket",
            "productRatingCount":17,
            "productAvgRating":4.9,
            "listPrice":16499.0,
            "minimumListPrice":16499.0,
            "maximumListPrice":16499.0,
            "promoPrice":8570.25,
            "minimumPromoPrice":8570.25,
            "maximumPromoPrice":8570.25,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"TCL",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1097746024.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1097746024.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1097746024.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1094820851",
            "skuRepositoryId":"1094820851",
            "productDisplayName":"Pantalla Samsung 4K UHD de 43 pulgadas, Modelo UN43TU8000FXZX",
            "productType":"Soft Line",
            "productRatingCount":238,
            "productAvgRating":4.8,
            "listPrice":15399.0,
            "minimumListPrice":15399.0,
            "maximumListPrice":15399.0,
            "promoPrice":9999.0,
            "minimumPromoPrice":9999.0,
            "maximumPromoPrice":9999.0,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"SAMSUNG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1094820851_0p.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1094820851_0p.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1094820851_0p.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1086877208",
            "skuRepositoryId":"1086877208",
            "productDisplayName":"Pantalla Hisense Full HD de 40 Pulgadas, Modelo 40H5500F",
            "productType":"Soft Line",
            "productRatingCount":108,
            "productAvgRating":4.4,
            "listPrice":10000.0,
            "minimumListPrice":10000.0,
            "maximumListPrice":10000.0,
            "promoPrice":6498.9975,
            "minimumPromoPrice":6498.9975,
            "maximumPromoPrice":6498.9975,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"HISENSE",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1086877208.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1086877208.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1086877208.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1096984533",
            "skuRepositoryId":"1096984533",
            "productDisplayName":"Pantalla Hisense 4K UHD de 43 pulgadas, Modelo 43H6500G",
            "productType":"Soft Line",
            "productRatingCount":30,
            "productAvgRating":4.9,
            "listPrice":18499.0,
            "minimumListPrice":18499.0,
            "maximumListPrice":18499.0,
            "promoPrice":7698.9975,
            "minimumPromoPrice":7698.9975,
            "maximumPromoPrice":7698.9975,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"HISENSE",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1096984533.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1096984533.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1096984533.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1098694061",
            "skuRepositoryId":"1098694061",
            "productDisplayName":"Xiaomi Mi TV Stick negro",
            "productType":"Soft Line",
            "productRatingCount":6,
            "productAvgRating":5.0,
            "listPrice":1699.0,
            "minimumListPrice":1699.0,
            "maximumListPrice":1699.0,
            "promoPrice":1199.0,
            "minimumPromoPrice":1199.0,
            "maximumPromoPrice":1199.0,
            "isHybrid":false,
            "marketplaceSLMessage":"Asian GadgetsVAsian GadgetseAsian GadgetsnAsian GadgetsdAsian GadgetsiAsian GadgetsdAsian GadgetsoAsian Gadgets Asian GadgetspAsian GadgetsoAsian GadgetsrAsian Gadgets Asian Gadgets{Asian Gadgets0Asian Gadgets}Asian Gadgets",
            "marketplaceBTMessage":"Asian GadgetsVAsian GadgetseAsian GadgetsnAsian GadgetsdAsian GadgetsiAsian GadgetsdAsian GadgetsoAsian Gadgets Asian GadgetspAsian GadgetsoAsian GadgetsrAsian Gadgets Asian Gadgets{Asian Gadgets0Asian Gadgets}Asian Gadgets",
            "isMarketPlace":true,
            "isImportationProduct":false,
            "brand":"XIAOMI",
            "seller":"Asian Gadgets",
            "category":"Streaming, Reproductores y Proyectores",
            "smImage":"https://ss629.liverpool.com.mx/sm/1098694061.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1098694061.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1098694061.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ],
            "variantsColor":[
               {
                  "colorName":"NEGRO",
                  "colorHex":"#000000",
                  "colorImageURL":""
               }
            ]
         },
         {
            "productId":"1094820843",
            "skuRepositoryId":"1094820843",
            "productDisplayName":"Pantalla Samsung 4K UHD de 55 pulgadas, Modelo UN55TU8000FXZX",
            "productType":"Big Ticket",
            "productRatingCount":93,
            "productAvgRating":4.8,
            "listPrice":21699.0,
            "minimumListPrice":21699.0,
            "maximumListPrice":21699.0,
            "promoPrice":14999.25,
            "minimumPromoPrice":14999.25,
            "maximumPromoPrice":14999.25,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"SAMSUNG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1094820843_0p.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1094820843_0p.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1094820843_0p.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1062795472",
            "skuRepositoryId":"1062795472",
            "productDisplayName":"Apple TV 4K 32 GB",
            "productType":"Soft Line",
            "productRatingCount":21,
            "productAvgRating":5.0,
            "listPrice":3999.0,
            "minimumListPrice":3999.0,
            "maximumListPrice":3999.0,
            "promoPrice":0.0,
            "minimumPromoPrice":0.0,
            "maximumPromoPrice":0.0,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"APPLE",
            "seller":"Liverpool",
            "category":"Streaming, Reproductores y Proyectores",
            "smImage":"https://ss628.liverpool.com.mx/sm/1062795472.jpg",
            "lgImage":"https://ss628.liverpool.com.mx/lg/1062795472.jpg",
            "xlImage":"https://ss628.liverpool.com.mx/xl/1062795472.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ],
            "variantsColor":[
               {
                  "colorName":"NEGRO",
                  "colorHex":"#000000",
                  "colorImageURL":""
               }
            ]
         },
         {
            "productId":"1094401085",
            "skuRepositoryId":"1094401085",
            "productDisplayName":"Pantalla Sony Full HD de 43 Pulgadas, Modelo KDL-43W660G",
            "productType":"Soft Line",
            "productRatingCount":20,
            "productAvgRating":4.7,
            "listPrice":11499.0,
            "minimumListPrice":11499.0,
            "maximumListPrice":11499.0,
            "promoPrice":8570.25,
            "minimumPromoPrice":8570.25,
            "maximumPromoPrice":8570.25,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"SONY",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1094401085.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1094401085.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1094401085.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1097881240",
            "skuRepositoryId":"1097881240",
            "productDisplayName":"Pantalla LG UHD TV AI ThinQ 4K de 49 pulgadas, Modelo 49UN7100PUA",
            "productType":"Big Ticket",
            "productRatingCount":4,
            "productAvgRating":4.5,
            "listPrice":17999.0,
            "minimumListPrice":17999.0,
            "maximumListPrice":17999.0,
            "promoPrice":9642.0,
            "minimumPromoPrice":9642.0,
            "maximumPromoPrice":9642.0,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"LG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1097881240.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1097881240.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1097881240.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1082823251",
            "skuRepositoryId":"1082823251",
            "productDisplayName":"Pantalla TCL HD de 32 pulgadas, Modelo 32A325",
            "productType":"Soft Line",
            "productRatingCount":115,
            "productAvgRating":4.8,
            "listPrice":6999.0,
            "minimumListPrice":6999.0,
            "maximumListPrice":6999.0,
            "promoPrice":4605.75,
            "minimumPromoPrice":4605.75,
            "maximumPromoPrice":4605.75,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"TCL",
            "seller":"Liverpool",
            "category":"Aparatos Electrónicos",
            "smImage":"https://ss629.liverpool.com.mx/sm/1082823251.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1082823251.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1082823251.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1087350441",
            "skuRepositoryId":"1087350441",
            "productDisplayName":"Centro de TV Diper Aviano Contemporáneo de madera tabaco",
            "productType":"Big Ticket",
            "productRatingCount":33,
            "productAvgRating":4.4,
            "listPrice":7499.0,
            "minimumListPrice":7499.0,
            "maximumListPrice":7499.0,
            "promoPrice":5249.3,
            "minimumPromoPrice":5249.3,
            "maximumPromoPrice":5249.3,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"DIPER",
            "seller":"Liverpool",
            "category":"Muebles de T.V.",
            "smImage":"https://ss101.liverpool.com.mx/sm/1087350441.jpg",
            "lgImage":"https://ss101.liverpool.com.mx/lg/1087350441.jpg",
            "xlImage":"https://ss101.liverpool.com.mx/xl/1087350441.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ],
            "variantsColor":[
               {
                  "colorName":"TABACO",
                  "colorHex":"#5a300c",
                  "colorImageURL":""
               }
            ]
         },
         {
            "productId":"1082792275",
            "skuRepositoryId":"1082792275",
            "productDisplayName":"Pantalla Sony 4K HDR de 55\" Mod. XBR-55X800G, con Triluminos display Smart TV LED",
            "productType":"Big Ticket",
            "productRatingCount":75,
            "productAvgRating":4.8,
            "listPrice":24999.0,
            "minimumListPrice":24999.0,
            "maximumListPrice":24999.0,
            "promoPrice":14999.25,
            "minimumPromoPrice":14999.25,
            "maximumPromoPrice":14999.25,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"SONY",
            "seller":"Liverpool",
            "category":"Aparatos Electrónicos",
            "smImage":"https://ss629.liverpool.com.mx/sm/1082792275.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1082792275.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1082792275.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               {
                  "flagId":"isShipPleasant",
                  "flagMessage":"Cupón"
               }
            ]
         },
         {
            "productId":"1097744307",
            "skuRepositoryId":"1097744307",
            "productDisplayName":"Pantalla LG UHD TV AI ThinQ 4K de 70 pulgadas, Modelo 70UN7370PUC",
            "productType":"Big Ticket",
            "productRatingCount":4,
            "productAvgRating":4.8,
            "listPrice":36999.0,
            "minimumListPrice":36999.0,
            "maximumListPrice":36999.0,
            "promoPrice":21427.5,
            "minimumPromoPrice":21427.5,
            "maximumPromoPrice":21427.5,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"LG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1097744307.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1097744307.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1097744307.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1095166993",
            "skuRepositoryId":"1095166993",
            "productDisplayName":"Pantalla TCL 43 Pulgadas 4K UHD (Android TV) AI_in 43A423",
            "productType":"Soft Line",
            "productRatingCount":53,
            "productAvgRating":4.6,
            "listPrice":13499.0,
            "minimumListPrice":13499.0,
            "maximumListPrice":13499.0,
            "promoPrice":7499.25,
            "minimumPromoPrice":7499.25,
            "maximumPromoPrice":7499.25,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"TCL",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1095166993.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1095166993.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1095166993.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1095374693",
            "skuRepositoryId":"1095374693",
            "productDisplayName":"Pantalla Samsung 4K UHD de 65 pulgadas, Modelo UN65TU8500FXZX",
            "productType":"Big Ticket",
            "productRatingCount":62,
            "productAvgRating":4.5,
            "listPrice":24284.0,
            "minimumListPrice":24284.0,
            "maximumListPrice":24284.0,
            "promoPrice":18213.0,
            "minimumPromoPrice":18213.0,
            "maximumPromoPrice":18213.0,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"SAMSUNG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1095374693.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1095374693.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1095374693.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1085100404",
            "skuRepositoryId":"1085100404",
            "productDisplayName":"Pantalla LG 4K UHD AI ThinQ de 65 Pulgadas, Modelo 65UM7400PUA",
            "productType":"Big Ticket",
            "productRatingCount":87,
            "productAvgRating":4.8,
            "listPrice":21427.14,
            "minimumListPrice":21427.14,
            "maximumListPrice":21427.14,
            "promoPrice":16070.355,
            "minimumPromoPrice":16070.355,
            "maximumPromoPrice":16070.355,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"LG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1085100404.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1085100404.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1085100404.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1098005028",
            "skuRepositoryId":"1098005028",
            "productDisplayName":"Pantalla LG UHD TV AI ThinQ 4K de 60 pulgadas, Modelo 60UN7300PUA",
            "productType":"Big Ticket",
            "productRatingCount":1,
            "productAvgRating":5.0,
            "listPrice":23999.0,
            "minimumListPrice":23999.0,
            "maximumListPrice":23999.0,
            "promoPrice":14999.25,
            "minimumPromoPrice":14999.25,
            "maximumPromoPrice":14999.25,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"LG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1098005028.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1098005028.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1098005028.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1097881151",
            "skuRepositoryId":"1097881151",
            "productDisplayName":"Pantalla LG UHD TV AI ThinQ 4K de 55 pulgadas, Modelo 55UN7300PUC",
            "productType":"Big Ticket",
            "productRatingCount":3,
            "productAvgRating":5.0,
            "listPrice":19999.0,
            "minimumListPrice":19999.0,
            "maximumListPrice":19999.0,
            "promoPrice":12855.75,
            "minimumPromoPrice":12855.75,
            "maximumPromoPrice":12855.75,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"LG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1097881151.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1097881151.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1097881151.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         },
         {
            "productId":"1098192405",
            "skuRepositoryId":"1098192405",
            "productDisplayName":"Pantalla Seiki 39 Televisor LED V3 HDMI USB VGA SC-39HS950N",
            "productType":"Soft Line",
            "productRatingCount":12,
            "productAvgRating":4.8,
            "listPrice":4899.0,
            "minimumListPrice":4899.0,
            "maximumListPrice":4899.0,
            "promoPrice":3699.0,
            "minimumPromoPrice":3699.0,
            "maximumPromoPrice":3699.0,
            "isHybrid":false,
            "marketplaceSLMessage":"GRUPO DECMEVGRUPO DECMEeGRUPO DECMEnGRUPO DECMEdGRUPO DECMEiGRUPO DECMEdGRUPO DECMEoGRUPO DECME GRUPO DECMEpGRUPO DECMEoGRUPO DECMErGRUPO DECME GRUPO DECME{GRUPO DECME0GRUPO DECME}GRUPO DECME",
            "marketplaceBTMessage":"GRUPO DECMEVGRUPO DECMEeGRUPO DECMEnGRUPO DECMEdGRUPO DECMEiGRUPO DECMEdGRUPO DECMEoGRUPO DECME GRUPO DECMEpGRUPO DECMEoGRUPO DECMErGRUPO DECME GRUPO DECME{GRUPO DECME0GRUPO DECME}GRUPO DECME",
            "isMarketPlace":true,
            "isImportationProduct":false,
            "brand":"SEIKO",
            "seller":"GRUPO DECME",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1098192405.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1098192405.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1098192405.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ],
            "variantsColor":[
               {
                  "colorName":"NEGRO",
                  "colorHex":"#000000",
                  "colorImageURL":""
               }
            ]
         },
         {
            "productId":"1094820746",
            "skuRepositoryId":"1094820746",
            "productDisplayName":"Pantalla Samsung 4K UHD de 75 pulgadas, Modelo UN75TU8000FXZX",
            "productType":"Big Ticket",
            "productRatingCount":27,
            "productAvgRating":4.6,
            "listPrice":46199.0,
            "minimumListPrice":46199.0,
            "maximumListPrice":46199.0,
            "promoPrice":27855.75,
            "minimumPromoPrice":27855.75,
            "maximumPromoPrice":27855.75,
            "isHybrid":false,
            "isMarketPlace":false,
            "isImportationProduct":false,
            "brand":"SAMSUNG",
            "seller":"Liverpool",
            "category":"Pantallas",
            "smImage":"https://ss629.liverpool.com.mx/sm/1094820746_0p.jpg",
            "lgImage":"https://ss629.liverpool.com.mx/lg/1094820746_0p.jpg",
            "xlImage":"https://ss629.liverpool.com.mx/xl/1094820746_0p.jpg",
            "groupType":"Not Specified",
            "plpFlags":[
               
            ]
         }
      ],
      "navigation":{
         "ancester":[
            {
               "label":"Cómputo y Electrónica",
               "categoryId":"cat5150041"
            },
            {
               "label":"Casa",
               "categoryId":"catst8233783"
            },
            {
               "label":"Muebles",
               "categoryId":"cat860739"
            },
            {
               "label":"Otras Categorias",
               "categoryId":"catst16656479"
            },
            {
               "label":"Cocina",
               "categoryId":"cat480186"
            },
            {
               "label":"Celulares",
               "categoryId":"cat4450173"
            },
            {
               "label":"Halloween y Navidad",
               "categoryId":"catst21474918"
            },
            {
               "label":"Belleza",
               "categoryId":"cat5020010"
            },
            {
               "label":"Bebés 0 - 24 Meses",
               "categoryId":"catst5620308"
            },
            {
               "label":"Línea Blanca",
               "categoryId":"cat610025"
            },
            {
               "label":"Videojuegos",
               "categoryId":"cat670055"
            },
            {
               "label":"Relojes, Lentes y Joyería",
               "categoryId":"cat4570008"
            },
            {
               "label":"Juguetes",
               "categoryId":"cat1080656"
            }
         ],
         "current":[
            {
               "label":"",
               "categoryId":""
            }
         ],
         "childs":[
            
         ]
      }
   }
}
*/
