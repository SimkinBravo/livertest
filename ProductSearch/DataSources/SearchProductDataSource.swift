//
//  DataSource.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

import UIKit

class SearchProductDataSource: NSObject {

    public var searchDataDidChange: ((String) -> Void)?
    public var previousSearchesViewDidRequest: (() -> Void)?
    public var collectionUpdate: (() -> Void)?
    public var loadNextPage: ((Int) -> Void)?

    private var products: [SearchProduct]?
    private var searchKeyword: String?

    private var headerReuseId: String = ProductSearchHeaderView.reuseId

    private var loadedElementsCount = 0

    var currentPage = 1
    let numberOfItemsPerPage = 10

    var numberOfSections: Int {

        return 1
    }

    func getNumberOfItems(for section: Int) -> Int {

        guard let number = products?.count else { return 0 }
        return number
    }

    func registerView(for collectionView: UICollectionView) {

        collectionView.register(SearchProductCollectionViewCell.self, forCellWithReuseIdentifier: SearchProductCollectionViewCell.reuseId)
        collectionView.register(ProductSearchHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ProductSearchHeaderView.reuseId)
    }

    func set(products: [SearchProduct]) {

        self.products = products
        collectionUpdate?()
    }

    func add(products: [SearchProduct]) {

        loadedElementsCount += 1
        self.products?.append(contentsOf: products)
        collectionUpdate?()
    }

}

extension SearchProductDataSource: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {

        return numberOfSections
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return getNumberOfItems(for: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        print(indexPath.row)
        if indexPath.row == (currentPage * numberOfItemsPerPage) - 1 {
            print("load next")
            currentPage += 1
            loadNextPage?(currentPage)
        }

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchProductCollectionViewCell.reuseId, for: indexPath)
        guard let customCell = cell as? SearchProductCollectionViewCell, let products = products, indexPath.row < products.count else { return cell }
        let data = products[indexPath.row]
        ConfigureProductCellView().configure(view: customCell, with: data)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        switch kind {
        case UICollectionView.elementKindSectionHeader:
            guard let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: ProductSearchHeaderView.reuseId, for: indexPath) as? ProductSearchHeaderView else { return UIView() as! UICollectionReusableView }

            view.textfield.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
            view.searchButton.addTarget(self, action: #selector(searchButtonDidTouch), for: .touchUpInside)
            view.previousSearchesButton.addTarget(self, action: #selector(previousSearchesButtonDidTouch), for: .touchUpInside)
            return view
        default:
            return UIView() as! UICollectionReusableView
        }
    }

    @objc func searchButtonDidTouch() {

        guard let keyword = searchKeyword else { return }

        searchDataDidChange?(keyword)
    }

    @objc func previousSearchesButtonDidTouch() {

        previousSearchesViewDidRequest?()
    }

    @objc func textFieldDidChange(_ textField: UITextField) {

        guard let keyword = textField.text else { return }

        searchKeyword = keyword
    }
}
