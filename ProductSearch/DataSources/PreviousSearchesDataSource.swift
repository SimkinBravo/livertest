//
//  PreviousSearchesDataSource.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

import UIKit

class PreviousSearchesDataSource: UIViewController {

    public var previousSearchSelected: ((String) -> Void)?
    public var collectionUpdate: (() -> Void)?

    var previousSearches: PreviousSearches?
    private var searchKeyword: String?

    var numberOfSections: Int {

        return 1
    }

    func getNumberOfItems(for section: Int) -> Int {

        guard let number = previousSearches?.searches.count else { return 0 }
        return number
    }

    func registerView(for collectionView: UICollectionView) {

        collectionView.register(PreviousSearchCollectionViewCell.self, forCellWithReuseIdentifier: PreviousSearchCollectionViewCell.reuseId)
    }

    func set(previousSearches: PreviousSearches) {

        self.previousSearches = previousSearches
        collectionUpdate?()
    }

}

extension PreviousSearchesDataSource: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {

        return numberOfSections
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return getNumberOfItems(for: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PreviousSearchCollectionViewCell.reuseId, for: indexPath)
        guard let customCell = cell as? PreviousSearchCollectionViewCell, let previousSearches = previousSearches, indexPath.row < previousSearches.searches.count else { return cell }
        let search = previousSearches.searches[indexPath.row]
        ConfigurePreviousSearchCellView().configure(view: customCell, with: search)
        return cell
    }
}
