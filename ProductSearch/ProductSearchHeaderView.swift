//
//  ProductSearchHeaderView.swift
//  ProductSearch
//
//  Created by Simkin on 10/09/20.
//  Copyright © 2020 Simkin. All rights reserved.
//

import UIKit
import Cartography

class ProductSearchHeaderView: UICollectionReusableView {

    static public let reuseId = "product-search-crv"

    let textfield = UITextField()
    let searchButton = UIButton()
    let previousSearchesButton = UIButton()

    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = .white
        initElements()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Interface Builder is not supported!")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        fatalError("Interface Builder is not supported!")
    }

    private func initElements() {

        textfield.placeholder = "Nombre del producto"
        searchButton.setTitle("Buscar", for: .normal)
        searchButton.backgroundColor = .systemBlue
        previousSearchesButton.setTitle("Busquedas anteriores", for: .normal)
        previousSearchesButton.backgroundColor = .systemGray

        addSubview(textfield)
        addSubview(searchButton)
        addSubview(previousSearchesButton)

        let margin: CGFloat = 20

        constrain(textfield, searchButton, previousSearchesButton) { txf, search, previous in

            let container = txf.superview!

            txf.left == container.left + margin
            txf.right == search.left - margin
            txf.top == container.top + margin
            txf.height == 40

            search.width == 100
            search.right == container.right - margin
            search.top == txf.top
            search.bottom == txf.bottom

            previous.left == txf.left
            previous.right == search.right
            previous.top == txf.bottom + margin
            previous.height == 40
        }
    }
}
